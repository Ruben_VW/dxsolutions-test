<tr>
    <td>{{$location->name}}</td>
    <td>{{$location->type}}</td>
    <td>{{$location->city}}</td>
    <td>{{$location->country}}</td>
    <td><a href="{{$location->url}}">{{$location->url}}</a></td>
    <td>{{ HTML::linkRoute('locations.edit', 'Edit', array($location->id)) }}</td>
    <td>
        {{ Form::open(array('route' => array('locations.destroy', $location->id), 'method' => 'delete')) }}
        <button type="submit" class="btn btn-danger btn-mini">Delete</button>
        {{ Form::close() }}
    </td>
</tr>