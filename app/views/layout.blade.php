<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">

    <title></title>
    <meta name="description" content="">
    <meta name="keywords" content="" />

    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
    <link href="{{asset('style/css/style.css')}}" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>

<div id="page">
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
</div>
@include('partials.scripts')
</body>
</html>