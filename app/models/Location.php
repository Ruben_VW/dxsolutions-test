<?php
/**
 * Created by PhpStorm.
 * User: RubenVW
 * Date: 16/11/14
 * Time: 13:04
 */

class Location extends Eloquent {

    protected $fillable = array('name', 'user_id', 'type', 'city', 'country', 'url');

    public static $rules = array(
        'name' => 'regex:/^([1-9a-zA-Z\.\s\-])+$/|required',
        'type' => 'regex:/^([a-zA-Z\.\s\-])+$/',
        'city' => 'regex:/^([a-zA-Z\.\s\-])+$/',
        'country' => 'regex:/^([a-zA-Z\.\s\-])+$/',
        'url' => 'url',
    );

    public static $messages = array(
        'name:regex' => 'no valid name',
        'type:regex' => 'no valid type',
        'city:regex' => 'no valid city name',
        'country:regex' => 'no valid country name',
        'url' => 'no valid url format'
    );

    public function getRules()
    {
        return self::$rules;
    }

    public function getMessages()
    {
        return self::$messages;
    }

} 