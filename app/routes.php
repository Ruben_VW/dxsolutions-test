<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));

Route::group(array('prefix' => 'users'), function(){

    Route::get('login', array('as' => 'login', 'uses' => 'UserController@loginForm'))->before('guest');
    Route::post('login', 'UserController@login');
    Route::get('register', array('as' => 'register', 'uses' => 'UserController@registerForm'))->before('guest');
    Route::post('register', 'UserController@register');
    Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@logout'))->before('auth');
    Route::get('profile', array('as' => 'profile', 'uses' => 'UserController@profile'))->before('auth');
    Route::put('edit/{id}', array('as' => 'users.edit', 'uses' => 'UserController@update'))->before('auth');

});

Route::group(array('prefix' => 'locations'), function(){
    Route::get('/', array('as' => 'locations.view', 'uses' => 'LocationController@view'))->before('auth');
    Route::get('edit/{id}', array('as' => 'locations.edit', 'uses' => 'LocationController@edit'))->before('auth');
    Route::put('update/{id}', array('as' => 'locations.update', 'uses' => 'LocationController@updateInternal'))->before('auth');
    Route::get('create', array('as' => 'locations.create', 'uses' => 'LocationController@create'))->before('auth');
    Route::post('store', array('as' => 'locations.store', 'uses' => 'LocationController@storeInternal'))->before('auth');
    Route::delete('destroy/{id}', array('as' => 'locations.destroy', 'uses' => 'LocationController@destroyInternal'))->before('auth');
});

Route::group(array('prefix' => 'api'), function(){

    Route::get('/', array('as' => 'api', function(){
        return View::make('api');
    }));

    //These require a registered username and password
    Route::group(array('before' => 'auth.basic'), function(){
        Route::get('users/detail', 'UserController@detail');
        Route::resource('users', 'UserController', array('except' => array('index', 'show')));
        Route::resource('locations', 'LocationController');
    });


    //These require an admin username and password
    Route::resource('users', 'UserController', array('only' => array('index', 'show')));

});

