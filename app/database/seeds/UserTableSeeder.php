<?php
/**
 * Created by PhpStorm.
 * User: RubenVW
 * Date: 15/09/14
 * Time: 17:14
 */

class UserTableSeeder extends Seeder  {
    public function run()
    {
        DB::table('users')->delete();

        $format = 'd-m-Y';

        User::create(array(
            'username' => 'test',
            'password' => Hash::make('test'),
            'street' => 'Roeselarestraat',
            'number' => '340',
            'zip' => '8560',
            'city' => 'Wevelgem',
            'birth' => DateTime::createFromFormat($format, '12-10-1993'),
        ));
    }
}
