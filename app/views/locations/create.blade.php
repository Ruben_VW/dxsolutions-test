@extends('layout')

@section('content')
<section class="container">
    <header>
        <h2>Create location</h2>
    </header>
    {{Form::open(array('route' => 'locations.store', 'method' => 'post'))}}
    <div class="form-group">
        {{Form::label('name', 'Name*')}} {{Form::text('name', '', array('class' => 'form-control'))}}
        @if($errors->first('name'))
        <div class="alert alert-danger" role="alert">{{$errors->first('name')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('type', 'Type')}} {{Form::text('type', '', array('class' => 'form-control'))}}
        @if($errors->first('type'))
        <div class="alert alert-danger" role="alert">{{$errors->first('type')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('city', 'City')}} {{Form::text('city', '', array('class' => 'form-control'))}}
        @if($errors->first('city'))
        <div class="alert alert-danger" role="alert">{{$errors->first('city')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('country', 'Country')}} {{Form::text('country', '', array('class' => 'form-control'))}}
        @if($errors->first('country'))
        <div class="alert alert-danger" role="alert">{{$errors->first('country')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('url', 'Url')}} {{Form::text('url', '', array('class' => 'form-control'))}}
        @if($errors->first('url'))
        <div class="alert alert-danger" role="alert">{{$errors->first('url')}}</div>
        @endif
    </div>

    <div class="form-group">
        {{Form::submit('save', array('class' => 'btn btn-default'))}}
    </div>
    {{Form::close()}}
</section>

@stop