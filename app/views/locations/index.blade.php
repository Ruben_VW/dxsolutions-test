@extends('layout')

@section('content')
<section class="container">
    <header>
        <h1>Your locations</h1>
    </header>
    <div class="panel panel-default">
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>City</th>
                <th>Country</th>
                <th>Url</th>
            </tr>

            @each('locations._item', $locations, 'location')
        </table>
    </div>
</section>
@stop