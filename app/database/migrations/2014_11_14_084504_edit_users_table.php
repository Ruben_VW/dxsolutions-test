<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table)
        {
            $table->string('street')->nullable();
            $table->string('number', 10)->nullable();
            $table->string('zip', 4)->nullable();
            $table->string('city', 100)->nullable();
            $table->timestamp('birth')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function($table)
        {
            $table->dropColumn('street');
            $table->dropColumn('number');
            $table->dropColumn('zip');
            $table->dropColumn('city');
        });
	}

}
