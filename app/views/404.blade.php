@extends('layout')

@section('content')
<section class="container">
    <header class="col-sm-6">
        <h1>Godmiljaar! 404!</h1>
        <h5>Ik haat page not found...</h5>
    </header>
    <figure class="col-sm-6">
        {{ HTML::image('style/img/kabouter-wesley.jpg', 'wesley', array('width' => '300', 'height' => '225')) }}
    </figure>
</section>


@stop