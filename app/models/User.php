<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    protected $fillable = array('username', 'password','street', 'number', 'zip', 'city', 'birth');

    public static $rules = array(
        'username' => 'unique:users',
        'old_password'  => 'passcheck',
        'password' => 'confirmed|required_with:old_password,username,password_confirmation',
        'street' => 'regex:/^([a-zA-Z\.\s\-])+$/',
        'number' => 'regex:/^([0-9a-zA-Z\s])+$/',
        'zip' => 'regex:/^([0-9]){4}+$/',
        'city' => 'regex:/^([a-zA-Z\.\s\-])+$/',
        'birth' => 'date'
    );

    public static $messages = array(
        'username.unique' => 'this username already exists',
        'old_password:passcheck' => 'old password was incorrect',
        'password:confirmed' => 'passwords are not equal',
        'password:required' => 'password is required',
        'street.regex' => 'no valid street name',
        'number.regex' => 'no valid number',
        'zip.regex' => 'zip must be a 4-numbered code',
        'city.regex' => 'no valid city name',
        'birth.date' => 'not a valid date'
    );

    public function getRules()
    {
        return self::$rules;
    }

    public function getMessages()
    {
        return self::$messages;
    }

    public function locations()
    {
        return $this->hasMany('Location');
    }

}
