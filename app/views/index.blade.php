@extends('layout')

@section('content')
<section class="container">
    <p>
        Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. <em>Lorem Ipsum</em> is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een <em>font-catalogus</em> te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren '60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals <em>Aldus PageMaker</em> die versies van Lorem Ipsum bevatten.
    </p>
</section>


@stop