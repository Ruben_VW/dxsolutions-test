<nav class="navbar-default navbar container">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
            <li>{{ HTML::linkRoute('index', 'Home') }}</li>
            <li>{{ HTML::linkRoute('api', 'Api') }}</li>
            @if(Auth::check())
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Locations <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>{{ HTML::linkRoute('locations.view', 'Your locations') }}</li>
                    <li>{{ HTML::linkRoute('locations.create', 'New location') }}</li>
                </ul>
            </li>
            <li>{{ HTML::linkRoute('profile', 'Profile' ) }}</li>
            <li>{{ HTML::linkRoute('logout', 'Logout ('.Auth::user()->username.')') }}</li>
            @else
            <li>{{ HTML::linkRoute('login', 'Login') }}</li>
            @endif
        </ul>
    </div>

</nav>

@if(Session::has('notice'))
<div class="container alert alert-info" role="alert">{{ Session::get('notice') }}</div>
@endif