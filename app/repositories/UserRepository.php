<?php
/**
 * Created by PhpStorm.
 * User: RubenVW
 * Date: 14/11/14
 * Time: 11:09
 */

class UserRepository extends BaseRepository {

    protected $model;
    protected $validator;

    public function __construct(User $model = null)
    {
        $this->model = $model ? : new User();
    }

    public function create(array $attributes){

        $valid = $this->validate($attributes, $this->model->getRules(), $this->model->getMessages());

        if ($valid) {
            $attributes['password'] = Hash::make($attributes['password']);
            return $this->model->create($attributes);
        }

        return $this->validator->messages()->getMessages();
    }

    public function update($id, array $attributes){

        $valid = $this->validate($attributes, $this->model->getRules(), $this->model->getMessages());

        if ($valid) {
            $model = $this->find($id);
            if(array_key_exists('password', $attributes)){
                if(array_key_exists('old_password', $attributes) && $attributes['old_password'] == ''){
                    $messages = $this->validator->messages()->getMessages();
                    $messages['old_password'] = ['Old password is required'];
                    return $messages;
                }
                $attributes['password'] = Hash::make($attributes['password']);
            }
            return $model->update($attributes);
        }

        return $this->validator->messages()->getMessages();
    }

    public function validate(array $attributes, array $rules, array $messages){

        Validator::extend('passcheck', function($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password); // Works for any form!
        });

        $this->validator = Validator::make($attributes, $rules, $messages);

        if (!$this->validator->fails()) {
            return true;
        }

        return false;
    }

    public function findWithLocations($id)
    {
        $user = $this->model->find($id);
        $user['locations'] = $user->locations;

        return $user;
    }

    public function findByUsername($username)
    {
        return $this->model->where('username', '=', $username)->first();
    }

} 