<?php
/**
 * Created by PhpStorm.
 * User: RubenVW
 * Date: 16/11/14
 * Time: 13:06
 */

class LocationRepository extends BaseRepository {

    protected $model;
    protected $validator;

    public function __construct(Location $model = null)
    {
        $this->model = $model ? : new Location();
    }

    public function delete($id)
    {
        $location = $this->model->where('user_id', '=', Auth::user()->id)->find($id);

        if($location){
            $location->delete();
            return true;
        }
        return false;
    }

    public function update($id, array $attributes)
    {
        $valid = $this->validate($attributes, $this->model->getRules(), $this->model->getMessages());

        if ($valid) {

            $model = $this->model->where('user_id', '=', Auth::user()->id)->find($id);

            if($model->update($attributes)){
                return $model;
            }

            return false;
        }

        return $this->validator->messages()->getMessages();
    }

    public function getPersonal()
    {
        return $this->model->where('user_id', '=', Auth::user()->id)->get();
    }

} 