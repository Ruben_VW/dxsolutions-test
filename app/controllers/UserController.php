<?php
/**
 * Created by PhpStorm.
 * User: RubenVW
 * Date: 14/11/14
 * Time: 10:20
 */

class UserController extends BaseController {

    protected $users;

    public function __construct (UserRepository $users) {
        $this->users = $users;
    }

    public function loginForm(){
        return View::make('users.login');
    }

    public function login(){
        $user = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        if(Auth::attempt($user)){
            return Redirect::route('index')
                ->withNotice('You are successfully logged in.');
        }

        // authentication failure! lets go back to the login page
        return Redirect::route('login')
            ->withError('Your username/password combination was incorrect.')
            ->withInput();
    }

    public function registerForm(){
        return View::make('users.register');
    }

    public function register(){
        Input::flash();

        $attributes = Input::all();
        $result = $this->users->create($attributes);

        if(gettype($result) != 'array'){
            $user = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );

            // authentication will not fail, because user is successfully created
            if(Auth::attempt($user)){
                return Redirect::route('index')
                    ->withNotice('You are successfully registered.');
            }
        }else{
            return Redirect::route('register')
                ->withErrors($result);
        }
    }

    public function logout(){
        Auth::logout();

        return Redirect::route('index')
            ->withNotice('You are successfully logged out.');
    }

    public function profile(){
        return View::make('users.profile');
    }

    public function update($id){
        Input::flash();

        $attributes = Input::all();
        $result = $this->users->update($id, $attributes);

        if(gettype($result) != 'array'){
            return Redirect::route('profile')
                ->withNotice('Gegevens zijn aangepast');
        }else{
            return Redirect::route('profile')
                ->withErrors($result);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index(){
        Httpauth::secure();

        return Response::json(
            array(
                'error' => false,
                'users' => $this->users->all()
            ), 200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function show($id){
        Httpauth::secure();

        return Response::json(
            array(
                'error' => false,
                'users' => $this->users->findWithLocations($id)
            ), 200
        );
    }

    public function detail(){

        $id = Request::get('id');
        $username = Request::get('username');

        if(!$id && !$username){
            return Response::json(
                array(
                    'error' => 'No id or username specified',
                    'users' => []
                ), 200
            );
        }

        if(!$id){
            $user = $this->users->findByUsername($username);

            if($user){
                $id = $user->id;
            }else{
                return Response::json(
                    array(
                        'error' => 'Invalid username',
                        'users' => []
                    ), 200
                );
            }
        }

        if(Auth::user()->id == $id){
            return Response::json(
                array(
                    'error' => false,
                    'users' => $this->users->find($id)
                ), 200
            );
        }else{
            return Response::json(
                array(
                    'error' => 'You have no permission to view this user\'s details',
                    'users' => []
                ), 200
            );
        }

    }

} 