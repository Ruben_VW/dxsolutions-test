var jsVendors = [
    'bower_components/jquery/jquery.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js'
];

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            main:{
                files: {
                    'public/style/css/style.css': 'dev/less/theme-default.less'
                },
                options:{
                    cleancss: true
                }
            }
        },

        watch: {
            files: [
                'dev/less/**/*.less',
                'dev/js/**/*.js',
                'bower_components/**'
            ],
            tasks: [
                'less',
                'uglify',
                'copy'
            ],
            options: {
                livereload: true
            }
        },

        uglify:{
            main:{
                options:{
                    wrap: true,
                    banner: '/* <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files:{
                    'public/js/app.js': [
                        'dev/js/**/*.js'
                    ]
                }
            }
        },

        copy:{
            main:{
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: jsVendors,
                        dest: 'public/js/vendor'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['less', 'uglify', 'copy']);

};