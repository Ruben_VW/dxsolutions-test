<?php

class BaseRepository{
    protected $model;
    protected $validator;

    public function __construct(Eloquent $model)
    {
        $this->model = $model ? : new Eloquent();
    }

    public function create(array $attributes)
    {
        $valid = $this->validate($attributes, $this->model->getRules(), $this->model->getMessages());

        if ($valid) {

            return $this->model->create($attributes);
        }

        return $this->validator->messages()->getMessages();
    }

    public function update($id, array $attributes)
    {
        $valid = $this->validate($attributes, $this->model->getRules(), $this->model->getMessages());

        if ($valid) {

            $model = $this->find($id);

            return $model->update($attributes);
        }

        return $this->validator->messages()->getMessages();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function validate(array $attributes, array $rules, array $messages)
    {

        $this->validator = Validator::make($attributes, $rules, $messages);

        if (!$this->validator->fails()) {
            return true;
        }

        return false;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function delete($id)
    {
        $this->model->destroy($id);
        return true;
    }

    public function getValidator()
    {
        return $this->validator;
    }
}