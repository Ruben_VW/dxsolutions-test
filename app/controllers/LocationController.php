<?php

class LocationController extends BaseController {

    protected $locations;

    public function __construct (LocationRepository $locations) {
        $this->locations = $locations;
    }

  	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return Response::json(
            array(
                'error' => false,
                'users' => $this->locations->all()
            ), 200
        );
	}

    public function view(){
        $personal_locations = $this->locations->getPersonal();
        return View::make('locations.index')->withLocations($personal_locations);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('locations.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $attributes = Input::all();
        $attributes['user_id'] = Auth::user()->id;

        $result = $this->locations->create($attributes);

        if(gettype($result) != 'array'){
            return Response::json(
                array(
                    'error' => false,
                    'locations' => $result
                ), 200
            );
        }else{
            return Response::json(
                array(
                    'error' => $result
                ), 200
            );
        }
	}

    public function storeInternal(){
        Input::flash();

        $result = $this->store();
        $decoded = json_decode($result->getContent(), true);

        if(!$decoded['error']){
            return Redirect::route('locations.view')
                ->withNotice('Location added');
        }else{
            return Redirect::route('locations.create')
                ->withErrors($decoded['error']);
        }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return Response::json(
            array(
                'error' => false,
                'locations' => $this->locations->find($id)
            ), 200
        );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $location = $this->locations->find($id);
        return View::make('locations.edit')->withLocation($location);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $attributes = Input::all();
        $result = $this->locations->update($id, $attributes);

        if(gettype($result) != 'array'){
            return Response::json(
                array(
                    'error' => false,
                    'locations' => $result
                ), 200
            );
        }else{
            return Response::json(
                array(
                    'error' => $result
                ), 200
            );
        }
	}

    public function updateInternal($id){
        Input::flash();

        $result = $this->update($id);
        $decoded = json_decode($result->getContent(), true);

        if(!$decoded['error']){
            return Redirect::route('locations.view')
                ->withNotice('Gegevens zijn aangepast');
        }else{
            return Redirect::route('locations.edit', array($id))
                ->withErrors($decoded['error']);
        }
    }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        if($this->locations->delete($id)){
            return Response::json(
                array(
                    'message' => 'Location deleted'
                ), 200
            );
        }else{
            return Response::json(
                array(
                    'error' => 'Location cannot be deleted'
                ), 200
            );
        }
	}

    public function destroyInternal($id){
        $result = $this->destroy($id);
        $decoded = json_decode($result->getContent(), true);

        if($decoded['message']){
            return Redirect::route('locations.view')
                ->withNotice('Location deleted');
        }else{
            return Redirect::route('locations.view', array($id))
                ->withNotice($decoded['error']);
        }
    }

}
