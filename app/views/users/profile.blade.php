@extends('layout')

@section('content')
<section class="container">
    <header>
        <h1>Welcome, {{ Auth::user()->username }}, to your profile!</h1>
    </header>

    <section class="col-sm-6">
        <header>
            <h2>Edit your data</h2>
        </header>
        {{Form::open(array('route' => array('users.edit', Auth::user()->id), 'method' => 'put'))}}
        <div class="form-group">
            {{Form::label('street', 'Street')}} {{Form::text('street', Auth::user()->street, array('class' => 'form-control'))}}
            @if($errors->first('street'))
            <div class="alert alert-danger" role="alert">{{$errors->first('phone')}}</div>
            @endif
        </div>
        <div class="form-group">
            {{Form::label('number', 'Nr.')}} {{Form::text('number', Auth::user()->number, array('class' => 'form-control'))}}
            @if($errors->first('number'))
            <div class="alert alert-danger" role="alert">{{$errors->first('number')}}</div>
            @endif
        </div>
        <div class="form-group">
            {{Form::label('zip', 'Zip')}} {{Form::text('zip', Auth::user()->zip, array('class' => 'form-control'))}}
            @if($errors->first('zip'))
            <div class="alert alert-danger" role="alert">{{$errors->first('zip')}}</div>
            @endif
        </div>
        <div class="form-group">
            {{Form::label('city', 'City')}} {{Form::text('city', Auth::user()->city, array('class' => 'form-control'))}}
            @if($errors->first('city'))
            <div class="alert alert-danger" role="alert">{{$errors->first('city')}}</div>
            @endif
        </div>
        <div class="form-group">
            {{Form::label('birth', 'Birth date')}} {{Form::date('birth', date("Y-m-d",strtotime(Auth::user()->birth)), array('class' => 'form-control'))}}
            @if($errors->first('birth'))
            <div class="alert alert-danger" role="alert">{{$errors->first('birth')}}</div>
            @endif
        </div>

        <div class="form-group">
            {{Form::submit('save', array('class' => 'btn btn-default'))}}
        </div>
        {{Form::close()}}
    </section>

    <section class="col-sm-6">
        <header>
            <h2>Change your password</h2>
        </header>

        {{Form::open(array('route' => array('users.edit', Auth::user()->id), 'method' => 'put'))}}

        <div class="form-group">

            {{Form::label('old_password', 'Old password*')}} {{Form::password('old_password', array('class' => 'form-control'))}}
            @if($errors->first('old_password'))
            <div class="alert alert-danger" role="alert">{{$errors->first('old_password')}}</div>
            @endif
            {{Form::label('password', 'New password*')}} {{Form::password('password', array('class' => 'form-control'))}}
            @if($errors->first('password'))
            <div class="alert alert-danger" role="alert">{{$errors->first('password')}}</div>
            @endif
            {{Form::label('password_confirmation', 'Retype new password')}} {{Form::password('password_confirmation', array('class' => 'form-control'))}}
            @if($errors->first('password_confirmation'))
            <div class="alert alert-danger" role="alert">{{$errors->first('password_confirmation')}}</div>
            @endif

        </div>

        <div class="form-group">
            {{Form::submit('save', array('class' => 'btn btn-default'))}}
        </div>
        {{Form::close()}}
    </section>


</section>
@stop