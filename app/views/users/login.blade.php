@extends('layout')

@section('content')
<section class="container">
    <header>
        <h1>Login</h1>
    </header>

    @if (Session::has('error'))
    <div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
    @endif

    {{ Form::open(array('route' => 'login', 'method' => 'POST')) }}

    <div class="form-group">
        {{ Form::label('username', 'Username') }}<br/>
        {{Form::text('username', '', array('class' => 'form-control'))}}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br/>
        {{ Form::password('password', array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::submit('Login', array('class' => 'btn btn-default')) }}
    </div>

    {{ Form::close() }}

    {{ HTML::linkRoute('register', 'Create a new account') }}

</section>

@stop