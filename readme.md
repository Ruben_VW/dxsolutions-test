# DX Test

An CRUD application to test my skills as a developer.

* MVC Framework: [Laravel](http://laravel.com)
	* Dependancies: [Composer](https://getcomposer.org/)
* Using Laravel's `Eloquent ORM`
* Front-end: [Twitter Bootstrap](http://getbootstrap.com/)
	* Package manager: [Bower](http://bower.io/)
	* Using [Grunt](http://gruntjs.com/)
* Items should be available via an `API`


## Before starting development

* `composer install` to install laravel dependancies
* `npm install` to install grunt files
* `bower install` to install bower files
* Don't forget to add your machine name in the `artisan` paths in the file `bootstrap/start.php`. This is to make it possible to migrate and seed the database with artisan.
* `grunt` to run grunt for first time
* `grunt watch` to run the grunt watcher

### When using Artisan

* `php artisan serve` to start artisan server
* Use an sqlite database
	* Uncomment `'default' => 'sqlite',` in the file `app/config/artisan/database.php`
* `php artisan migrate` to run database migrations
* `php artisan db:seed` to seed database

### When using Homestead

* Run `vagrant up`/`vagrant provision` in your Homestead folder to start the server
* Use MySQL (or PostgreSQL)
* `php artisan migrate` to run database migrations
* `php artisan db:seed` to seed database

This is my `homestead.yaml`:
	
```
---
ip: "192.168.10.10"
memory: 2048
cpus: 1

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: /path/to/laravel/projects/
      to: /home/vagrant/Code

sites:
    - map: homestead.dx-test
      to: /home/vagrant/Code/DXTest/public

variables:
    - key: APP_ENV
      value: local	
```

**Change `path/to/laravel/projects/` to the correct path**

**Check the ssh key paths**


## The Website

### When using Artisan

Surf to `localhost:8000`

### When using Homestead

Surf to `homestead.dx-test`


## The API

* For secured data, I use [HttpAuth](https://github.com/Intervention/httpauth).
* The API will return a `json` string of the desired data.

### When using Artisan

* Surf to `localhost:8000/api` to see all available url's
* If you want to use terminal: `curl -i http://localhost:8000/api` etc.
* For a secured path: `curl -i --user username:password http://localhost:8000/api`

### When using Homestead

* Surf to `homestead.dx-test/api` to see all available url's
* If you want to use terminal: `curl -i 192.168.10.10/api` etc.
* For a secured path: `curl -i --user username:password 192.168.10.10/api`