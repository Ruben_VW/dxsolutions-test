@extends('layout')

@section('content')
<section class="container">
    <header>
        <h1>All available API url's</h1>
        @if(!Auth::check())
        <div class="alert alert-warning" role="alert">You'll need to {{ HTML::linkRoute('login', 'login', array(), array('class' => 'alert-link')) }} to use the api!</div>
        @endif
    </header>

    <section>
        <header>
            <h2>Users Admin</h2>
            <div class="well well-sm">You'll need the admin username and password!</div>
        </header>

        <ul class="list-group">
            <li class="list-group-item">
                All users: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/users/</em>
            </li>
            <li class="list-group-item">
                User by id: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/users/{id}</em>
            </li>
        </ul>
    </section>

    <section>
        <header>
            <h2>Users</h2>
        </header>

        <ul class="list-group">
            <li class="list-group-item">
                User by id: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/users/detail?id={id}</em>
            </li>
            <li class="list-group-item">
                User by username: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/users/detail?username={username}</em>
            </li>
        </ul>
    </section>

    <section>
        <header>
            <h2>Locations</h2>
        </header>

        <ul class="list-group">
            <li class="list-group-item">
                All locations: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/locations</em>
            </li>
            <li class="list-group-item">
                Location by id: <span class="label label-primary">GET</span><br/>
                <em>homestead.dx-test/api/locations/{id}</em>
            </li>
            <li class="list-group-item">
                Add new location: <span class="label label-primary">POST</span><br/>
                <em>homestead.dx-test/api/locations</em><br/>
                <div class="panel panel-default">
                    <div class="panel-heading">Required data</div>
                    <table class="table">
                        <tr>
                            <th>name</th>
                            <td>Name of this location</td>
                        </tr>
                    </table>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Optional data</div>
                    <table class="table">
                        <tr>
                            <th>type</th>
                            <td>Location type (e.g. amusement park, museum)</td>
                        </tr>
                        <tr>
                            <th>city</th>
                            <td>The location's city</td>
                        </tr>
                        <tr>
                            <th>country</th>
                            <td>The location's country</td>
                        </tr>
                        <tr>
                            <th>url</th>
                            <td>Homepage of the location</td>
                        </tr>
                    </table>
                </div>
            </li>
            <li class="list-group-item">
                Update location: <span class="label label-primary">PUT</span><br/>
                <em>homestead.dx-test/api/locations</em><br/>
                <div class="panel panel-default">
                    <div class="panel-heading">Required data</div>
                    <table class="table">
                        <tr>
                            <th>name</th>
                            <td>Name of this location</td>
                        </tr>
                    </table>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Optional data</div>
                    <table class="table">
                        <tr>
                            <th>type</th>
                            <td>Location type (e.g. amusement park, museum)</td>
                        </tr>
                        <tr>
                            <th>city</th>
                            <td>The location's city</td>
                        </tr>
                        <tr>
                            <th>country</th>
                            <td>The location's country</td>
                        </tr>
                        <tr>
                            <th>url</th>
                            <td>Homepage of the location</td>
                        </tr>
                    </table>
                </div>
            </li>
            <li class="list-group-item">
                Delete location: <span class="label label-primary">DELETE</span><br/>
                <em>homestead.dx-test/api/locations/{id}</em>
            </li>
        </ul>
    </section>
</section>


@stop