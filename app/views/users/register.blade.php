@extends('layout')

@section('content')
<section class="container">
    <header>
        <h1>Create account</h1>
    </header>

    {{Form::open(array('route' => 'register', 'method' => 'post'))}}
    <div class="form-group">
        {{Form::label('username', 'Username*')}} {{Form::text('username', '', array('class' => 'form-control'))}}
        @if($errors->first('username'))
        <div class="alert alert-danger" role="alert">{{$errors->first('username')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('password', 'Password*')}} {{Form::password('password', array('class' => 'form-control'))}}
        @if($errors->first('password'))
        <div class="alert alert-danger" role="alert">{{$errors->first('password')}}</div>
        @endif
        {{Form::label('password_confirmation', 'Retype password*')}} {{Form::password('password_confirmation', array('class' => 'form-control'))}}
        @if($errors->first('password_confirmation'))
        <div class="alert alert-danger" role="alert">{{$errors->first('password_confirmation')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('street', 'Street')}} {{Form::text('street', '', array('class' => 'form-control'))}}
        @if($errors->first('street'))
        <div class="alert alert-danger" role="alert">{{$errors->first('street')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('number', 'Nr.')}} {{Form::text('number', '', array('class' => 'form-control'))}}
        @if($errors->first('number'))
        <div class="alert alert-danger" role="alert">{{$errors->first('number')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('zip', 'Zip')}} {{Form::text('zip', '', array('class' => 'form-control'))}}
        @if($errors->first('zip'))
        <div class="alert alert-danger" role="alert">{{$errors->first('zip')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('city', 'City')}} {{Form::text('city', '', array('class' => 'form-control'))}}
        @if($errors->first('city'))
        <div class="alert alert-danger" role="alert">{{$errors->first('city')}}</div>
        @endif
    </div>
    <div class="form-group">
        {{Form::label('birth', 'Birth date')}} {{Form::date('birth', '', array('class' => 'form-control'))}}
        @if($errors->first('birth'))
        <div class="alert alert-danger" role="alert">{{$errors->first('birth')}}</div>
        @endif
    </div>

    <div class="form-group">
        {{Form::submit('register', array('class' => 'btn btn-default'))}}
    </div>
    {{Form::close()}}

</section>
@stop